#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char *pswrd;
    char *hash;
};

// Comparison function, sort struct entries by hashes using qsort
int sortHash(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash, bb->hash);
}

// Comparison function for binary search
int hashComp(const void *key, const void *elem)
{
    char *kk = (char *)key;
    struct entry *ee = (struct entry *)elem;
    return strcmp(kk, ee->hash);
}

// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary word.
struct entry *read_dictionary(char *filename, int *size)
{
    // Use stat to find out how big the file is
    struct stat info;
    if (stat(filename, &info) == -1)
    {
        printf("Can't stat the file\n" );
        exit(1);
    }
    int filesize = info.st_size;
    
     // Allocate memory for the file, plus one for the null
    char *contents = malloc(filesize + 1);
    
    FILE *in = fopen(filename, "rb");
    if (!in)
    {
        printf("Can't open file for reading");
        exit(1);
    }
    fread(contents, 1, filesize, in);
    fclose(in);
    
     // Place null character on end to make it a string
    contents[filesize] = '\0';
    
    // Count the newlines
    int nlcount = 0;
    for (int i = 0; i < filesize; i++)
    {
        if (contents[i] == '\n') nlcount++;
    }
    
    // Allocate memory for array of struct entries
    struct entry *arr = malloc(nlcount * sizeof(struct entry));
    
    // Fill in array of string pointers
    arr[0].pswrd = strtok(contents, "\n");
    int i = 1;
    while ((arr[i].pswrd = strtok(NULL, "\n")) != NULL)
    {
        i++;
    }
    
    // Fill in array of hash pointers
    for (int i = 0; i < nlcount; i++)
    {
        arr[i].hash = md5(arr[i].pswrd, strlen(arr[i].pswrd));
    }
    
    /* Print array
    for (int i = 0; i < nlcount; i++)
    {
        printf("%d %s %s\n", i, arr[i].pswrd, arr[i].hash);
    }*/

    // Return values
    *size = nlcount;
    return arr;
    
    // Key variables
    // contents = pointer to the entire file
    // arr = pointer to an array of struct entries
    // nlcount = number of strings
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of struct entries
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    char *free_dict = dict[0].pswrd;
    
    // Sort the hashed dictionary using qsort.
    qsort(dict, dlen, sizeof(struct entry), sortHash);
    
    /* Print array
    for (int i = 0; i < dlen; i++)
    {
        printf("%d %s %s\n", i, dict[i].pswrd, dict[i].hash);
    }*/

    // Open the hash file for reading.
    FILE *h = fopen(argv[1], "r");
    if (!h)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    
    char hash[HASH_LEN];
    int count = 0;
    while (fgets(hash, HASH_LEN, h) != NULL)        // Loop through hashfile
    {
        struct entry * found = 
            bsearch(hash, dict, dlen, sizeof(struct entry), hashComp);      // Binary search
            
        if (found)
        {
            // Print the password and hash in a numbered list
            printf("%d. %s -> %s\n",count, found->pswrd, found->hash);
            count++;
        }
    }
    printf("Found %d matches from %s\n", count, argv[2]);
    
    // Free memory
    fclose(h);
    free(free_dict);
    for (int i = 0; i < dlen; i++)
    {
        free(dict[i].hash);
    }
    free(dict);
}
